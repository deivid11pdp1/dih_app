import 'package:intl/intl.dart';

class User {
  late String? username;
  late String? password;
  late int? number;
  late Map? cobros;
  User({this.username, this.password, this.cobros});
}
