class NewSaleEntity {
  late String cliente;
  late String servicio;
  late int precio;
  late String? typeSuscription = "Por favor seleccione el servicio :)";
  late DateTime fechaPago = DateTime.now();
  late int cel = 31556565654;
  late int sumatoFecha;

  Map<String, dynamic> returnmap() {
    return {
      "Cliente": cliente,
      "Precio": precio,
      "WhastApp": cel,
      "Servicio": servicio,
      "Fecha Pago": fechaPago.add(Duration(days: sumatoFecha, hours: 20)),
      "Tipo de Inscripcion": typeSuscription,
    };
  }
}
