import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntityDates {
  late String? cliente;
  late String? servicio;
  late int? precio;
  late String? typeSuscription;
  late Timestamp? fechaPago;
  late int? cel;
  late dynamic id_Cliente;

  get getIdCliente => this.id_Cliente;

  set setIdCliente(value) => this.id_Cliente = value;
  get getCliente => this.cliente;

  set setCliente(cliente) => this.cliente = cliente;

  get getServicio => this.servicio;

  set setServicio(servicio) => this.servicio = servicio;

  get getPrecio => this.precio;

  set setPrecio(precio) => this.precio = precio;

  get getTypeSuscription => this.typeSuscription;

  set setTypeSuscription(typeSuscription) =>
      this.typeSuscription = typeSuscription;

  get getFechaPago => DateTime.fromMicrosecondsSinceEpoch(
      this.fechaPago!.microsecondsSinceEpoch);

  set setFechaPago(fechaPago) => this.fechaPago = fechaPago;

  get getCel => this.cel;

  set setCel(cel) => this.cel = cel;

  UserEntityDates(
      {this.cel,
      this.cliente,
      this.precio,
      this.servicio,
      this.typeSuscription,
      this.fechaPago,
      this.id_Cliente});
  String returnStringFecha() {
    return "${getFechaPago.year}-${getFechaPago.month}-${getFechaPago.day}";
  }

  UserEntityDates.fromJson(Map<String, dynamic> json, String id) {
    id_Cliente = id;
    cliente = json['Cliente'];
    servicio = json['Servicio'];
    if (json.containsKey("Fecha Pago")) fechaPago = json['Fecha Pago'];
    if (json.containsKey("Tipo de Inscripcion")) {
      typeSuscription = json['Tipo de Inscripcion'];
    }
    precio = json['Precio'];
    cel = json['WhastApp'];
  }

  factory UserEntityDates.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();
    // ignore: unnecessary_new
    return new UserEntityDates(
      cel: data?['cel'],
      cliente: data?['cliente'],
      servicio: data?['servicio'],
      precio: data?['precio'],
      typeSuscription: data?['typeSuscription'],
      fechaPago: data?['fechaPago'],
      id_Cliente: data?["id"],
    );
  }

  Map<String, dynamic> toFireStore() {
    return {
      if (cel != null) "cel": cel,
      if (cliente != null) "cliente": cliente,
      if (servicio != null) "servicio": servicio,
      if (precio != null) "precio": precio,
      if (typeSuscription != null) "typeSuscription": typeSuscription,
      if (fechaPago != null) "fechaPago": fechaPago
    };
  }
}
