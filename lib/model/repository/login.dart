import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dihapp/model/entity/UserEntity.dart';
import 'package:dihapp/model/entity/user.dart';
import 'package:firebase_core/firebase_core.dart';

class UserRepository {
  final _users = <String, User>{};
  final db = FirebaseFirestore.instance;
  late UserEntityDates userDates;
  UserRepository() {
    _users["deivid11pdp1@gmail.com"] = User(
      username: "Deivid",
      password: "12345",
      cobros: {
        'Valentina': ["Netflix", '20.000', "18/11/2022"],
        'Sara': ["Spotify", '28.000', "18/11/2022"]
      },
    );
    _users["julieth@dihapp.com"] = User(
      username: "Julieth",
      password: "12345",
      cobros: {
        'Valentina': ["Netflix", 20.000, "18/11/2022"],
      },
    );
  }

  User findUserName(String username) {
    var user = _users[username];

    if (user == null) {
      throw Exception("Usuario no existente");
    }
    return user;
  }

  User getDateUser(String username) {
    var user = _users[username];

    if (user == null) {
      throw Exception("Usuario no existente");
    }
    return user;
  }

  Future<List> getUserModel(String user) async {
    List<UserEntityDates> lists = [];

    dynamic userData = await db
        .collection("users")
        .doc(user)
        .collection("Clientes")
        .snapshots()
        .listen((event) {
      event.docs.forEach((DocumentSnapshot doc) {
        lists.add(UserEntityDates.fromJson(doc.data() as Map<String, dynamic>, doc.reference.id));
      
        //  userDates = UserEntityDates(cel: element["WhasApp"], cliente: element["Cliente"], precio: element["Precio"], servicio: element["Servicio"], typeSuscription: element["Tipo de Inscripcion"], fechaPago: element["Fecha Pago"]);
      });
    });
    print(lists);
    return lists;
  }

  Future<void> updatePaymentClient(
      DateTime? time, String? email, cliente) async {
    dynamic result = await db
        .collection("users")
        .doc("$email")
        .collection("Clientes")
        .doc("$cliente");
    result.update({"Fecha Pago": time}).then(
        (value) => print("DocumentSnapshot Successfully updated!"),
        onError: (result) => Future.error(result.error));
    return result;
  }
}
