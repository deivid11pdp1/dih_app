import 'package:dihapp/controller/request/LoginRequest.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthenticationRepository {
  Future<void> signInApp(String email, String password) async {
    try {
      var validation = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      print(validation);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return Future.error('Correo Electronico no existente .I. ');
      } else if (e.code == 'wrong-password') {
        return Future.error('Contraseña Incorrecta fokiu.');
      }
    }
  }
}
