import 'package:dihapp/controller/request/LoginRequest.dart';
import 'package:dihapp/model/entity/UserEntity.dart';
import 'package:dihapp/model/repository/fb_auth.dart';

import '../model/repository/login.dart';

class LoginController {
  late final UserRepository _userRepository;
  late LoginRequest _loginRequest;
  late FirebaseAuthenticationRepository _firebaseAuthentication;
  LoginController() {
    _userRepository = UserRepository();
    _firebaseAuthentication = FirebaseAuthenticationRepository();
  }
  String validarCorreoClave(LoginRequest request) {
    //Consultar usuario

    var user = _userRepository.findUserName(request.email.toString());
    //Verificar si la clave es igual
    if (user.password != request.password) {
      throw Exception("La contraseña es incorrecta");
    }
    return user.username!;
  }

  Future<void> controllerSignup(LoginRequest _loginRequest) async {
    await _firebaseAuthentication.signInApp(
        _loginRequest.email.toString(), _loginRequest.password.toString());
  }

  Future<List> getUserDates(String name) async {
    try {
      final userDates = await UserRepository().getUserModel(name);

      return userDates;
    } catch (e) {
      return Future.error(e);
    }
  }
}
