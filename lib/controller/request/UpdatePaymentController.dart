import 'package:dihapp/model/repository/login.dart';

class UpdatePaymentRequest {
  DateTime? time;
  String? email;
  late UserRepository _userRepository;
  UpdatePaymentRequest(){
    _userRepository = UserRepository();
  }
String? client;
  Future<void> updatePaymentRequestController(DateTime time, String email, String client) async {
    try{
   return await _userRepository.updatePaymentClient(
        time, email, client);
    }catch(e){
         return Future.error(e);
    }
  }
}
