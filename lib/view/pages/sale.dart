import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dihapp/model/entity/newsale.dart';
import 'package:dihapp/model/repository/fb_auth.dart';
import 'package:dihapp/view/widgets/Drawer_home.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';

class NewSale extends StatefulWidget {
  String? email;
  NewSale({super.key, required this.email});

  @override
  State<NewSale> createState() => _NewSaleState();
}

Widget _BuildAppBarSale() {
  return AppBar(
    elevation: 15,
    backgroundColor: const Color.fromARGB(255, 53, 0, 114),
    leadingWidth: 46,
    title: const Align(
      alignment: Alignment.topLeft,
      child: Text('Ventas'),
    ),
    actions: [
      Row(
        children: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              print("");
            },
          ),
          // IconButton(
          //   icon: const Icon(Icons.add),
          //   onPressed: () {
          //     print('Click en More');
          //   },
          // ),
        ],
      )
    ],
  );
}

class _NewSaleState extends State<NewSale> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: _BuildAppBarSale(),
      ),
      drawer: Drawerclass(
        nombreusuario: 'Deivid',
        email: "Prueba desde sale.dart",
      ),
      body: FormSale(
        email: widget.email,
      ),
    );
  }
}

class FormSale extends StatefulWidget {
  String? email;
  FormSale({super.key, required this.email});
  @override
  State<FormSale> createState() => _FormSaleState();
}

class _FormSaleState extends State<FormSale> {
  final _formKey = GlobalKey<FormState>();

  late dynamic _entity = new NewSaleEntity();

  late dynamic basedate = FirebaseFirestore.instance;
  DateTime? _selectedDate;
  String _type_Suscription = "Seleccione el tipo de Suscripcion";
  String _type_Servicio = "Seleccione el tipo de Servicio";
  int _sumatoPayment = 0;
  Widget _buildTextFieldName() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: TextFormField(
        cursorColor: const Color.fromARGB(255, 88, 0, 143),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: InputDecoration(
            prefixIcon: const Icon(
              Icons.person_add,
              color: Colors.deepPurpleAccent,
            ),
            filled: true,
            fillColor: const Color.fromARGB(255, 211, 211, 211),
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(50)),
            hintText: "Nombre del cliente"),
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Por favor ingrese un nombre';
          }
          return null;
        },
        onSaved: (e) => _entity.cliente = e,
      ),
    );
  }

  Widget _buildTextFieldService() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: TextFormField(
        keyboardType: TextInputType.number,
        cursorColor: const Color.fromARGB(255, 88, 0, 143),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: InputDecoration(
            prefixIcon: const Icon(
              Icons.monetization_on_sharp,
              color: Colors.deepPurpleAccent,
            ),
            filled: true,
            fillColor: const Color.fromARGB(255, 211, 211, 211),
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(50)),
            hintText: "Valor del Servicio"),
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Por favor ingrese un Servicio';
          }
          return null;
        },
        onSaved: (newValue) => _entity.precio = int.parse(newValue.toString()),
      ),
    );
  }

  Widget _buildTextFieldCel() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: TextFormField(
        keyboardType: TextInputType.number,
        cursorColor: const Color.fromARGB(255, 88, 0, 143),
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
        decoration: InputDecoration(
            prefixIcon: const Icon(
              Icons.whatsapp_sharp,
              color: Colors.deepPurpleAccent,
            ),
            filled: true,
            fillColor: const Color.fromARGB(255, 211, 211, 211),
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(50)),
            hintText: "Numero de WhastApp"),
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Por favor ingrese el Whastapp del cliente';
          }
          return null;
        },
        onSaved: (newValue) => _entity.cel = int.parse(newValue.toString()),
      ),
    );
  }

  Widget _buildDropDownSuscription() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: DropdownButtonFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(50)),
          prefixIcon: const Icon(
            Icons.note_add_sharp,
            color: Colors.deepPurpleAccent,
          ),
          filled: true,
          fillColor: const Color.fromARGB(255, 211, 211, 211),
        ),
        dropdownColor: const Color.fromARGB(255, 243, 243, 243),
        style: const TextStyle(color: Color.fromARGB(255, 72, 72, 72)),
        value: _type_Suscription,
        onChanged: (String? newValue) {
          switch (newValue) {
            case "Mensual":
              setState(() {
                _sumatoPayment = 30;
              });
              break;
            case "Semanal":
              setState(() {
                _sumatoPayment = 7;
              });
              break;
            case "Anual":
              setState(() {
                _sumatoPayment = 365;
              });
          }
          setState(() {
            _type_Suscription = newValue!;
            _entity.typeSuscription = _type_Suscription;
            _entity.sumatoFecha = _sumatoPayment;
          });
        },
        items: <String>[
          'Seleccione el tipo de Suscripcion',
          'Mensual',
          'Semanal',
          'Anual'
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: const TextStyle(fontSize: 16),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildDropDownService() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: DropdownButtonFormField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(50)),
          prefixIcon: const Icon(
            Icons.note_add_sharp,
            color: Colors.deepPurpleAccent,
          ),
          filled: true,
          fillColor: const Color.fromARGB(255, 211, 211, 211),
        ),
        dropdownColor: const Color.fromARGB(255, 243, 243, 243),
        style: const TextStyle(color: Color.fromARGB(255, 72, 72, 72)),
        value: _type_Servicio,
        onChanged: (String? newValue) {
          setState(() {
            _type_Servicio = newValue!;
            _entity.servicio = _type_Servicio;
          });
        },
        items: <String>[
          'Seleccione el tipo de Servicio',
          'Netflix ',
          'Spotify',
          'Disney Plus'
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: const TextStyle(fontSize: 16),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildCalendaryForm() {
    return Padding(
        padding: const EdgeInsets.only(top: 20),
        child: GestureDetector(
          onTap: () {
            showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(DateTime.now().year - 100),
                    lastDate: DateTime(DateTime.now().year + 6))
                .then((value) {
              setState(() {
                _selectedDate = value!;

                _entity.fechaPago = _selectedDate;
              });
            });
          },
          child: TextField(
            enabled: false,
            cursorColor: const Color.fromARGB(255, 88, 0, 143),
            style: const TextStyle(color: Color.fromARGB(255, 64, 0, 146)),
            decoration: InputDecoration(
                filled: true,
                fillColor: const Color.fromARGB(255, 211, 211, 211),
                border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(50)),
                hintText:
                    '${(_selectedDate == null) ? "Seleccione la fecha de pago ${_type_Suscription}" : DateFormat.yMMMEd().format(_selectedDate!.add(Duration(days: _sumatoPayment)))}',
                hintStyle: const TextStyle(color: Colors.black),
                prefixIcon: const Icon(Icons.calendar_month)),

            // The validator receives the text that the user has entered.
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 26),
          child: Text(
            "Nueva Venta",
            style: TextStyle(fontSize: 26, fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTextFieldName(),
                _buildTextFieldService(),
                _buildTextFieldCel(),
                _buildDropDownService(),
                _buildDropDownSuscription(),
                _buildCalendaryForm(),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromARGB(255, 58, 0, 125),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          textStyle: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          Map result = await _entity.returnmap();
                          basedate
                              .collection("users")
                              .doc("${widget.email}")
                              .collection("Clientes")
                              .doc()
                              .set(result)
                              .then((e) {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text('Cliente Agregado Correctamente'),
                              backgroundColor:
                                  Color.fromARGB(255, 98, 249, 125),
                            ));
                            Navigator.pop(context);
                          },
                                  onError: (e) => ScaffoldMessenger.of(context)
                                      .showSnackBar(const SnackBar(
                                          content: Text(
                                              "Hubo un error Comuniquese con el Administrador"),
                                          backgroundColor: Color.fromARGB(
                                              255, 98, 249, 125))));

                          // Validate returns true if the form is valid, or false otherwi

                        }

// Add a new document with a generated ID
                      },
                      child: const Text('Agregar'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 20, right: 10),
              child: FloatingActionButton(
                backgroundColor: Color.fromARGB(255, 72, 0, 167),
                onPressed: (() {
                  Navigator.pop(context);
                }),
                child: Icon(Icons.arrow_back_ios_new),
              ),
            )
          ],
        )
      ],
    );
  }
}
