import 'package:dihapp/view/widgets/Appbar_home.dart';
import 'package:dihapp/view/widgets/Drawer_home.dart';
import 'package:flutter/material.dart';

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({super.key});

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Pagina 1',
      style: optionStyle,
    ),
    Text(
      'Index 1:  Pagina 2',
      style: optionStyle,
    ),
    Text(
      'Index 2:  Pagina 3 ',
      style: optionStyle,
    ),
    Text(
      'Index 3:  Pagina 4',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawerclass(nombreusuario: "Deivid", email: "Prueba desde home.dart",),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppbarInsert(title: "Home"),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Inicio',
            backgroundColor: Color.fromARGB(255, 53, 0, 114),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_location_alt_rounded),
            label: 'Ubicación',
            backgroundColor: Color.fromARGB(255, 53, 0, 114),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.app_registration),
            label: 'Pedidos',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Configuración',
            backgroundColor: Color.fromARGB(255, 53, 0, 114),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color.fromARGB(255, 0, 234, 255),
        onTap: _onItemTapped,
      ),
    );
  }
}
