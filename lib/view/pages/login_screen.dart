import 'package:dihapp/controller/login.dart';
import 'package:dihapp/controller/request/LoginRequest.dart';
import 'package:dihapp/view/pages/charges.dart';

import 'package:flutter/material.dart';

import '../../model/entity/UserEntity.dart';
import '../../model/entity/user.dart';
import '../../model/repository/login.dart';
import '../../utilities/constants.dart';

class Loginscreen extends StatefulWidget {
  late UserEntityDates userClients;
  late LoginController _controller;
  late LoginRequest _request;
  late UserRepository _userRepository;
  Loginscreen({super.key}) {
    userClients = UserEntityDates();
    _controller = LoginController();
    _request = LoginRequest();
    _userRepository = UserRepository();
  }

  @override
  State<Loginscreen> createState() => _LoginscreenState();
}

class _LoginscreenState extends State<Loginscreen> {
  final _formKey = GlobalKey<FormState>();
  bool _rememberMe = false;

  Widget _BuildEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            onChanged: (text) {},
            keyboardType: TextInputType.emailAddress,
            style: const TextStyle(color: Colors.white, fontFamily: 'OpenSans'),
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.only(top: 14.0),
                prefixIcon: const Icon(
                  Icons.email,
                  color: Colors.white,
                ),
                hintText: 'Ingresa tu Email',
                hintStyle: kHintTextStyle),
            onSaved: (newValue) {
              widget._request.email = newValue!;
            },
          ),
        )
      ],
    );
  }

  Widget _BuildRememberCheckbox() {
    return Container(
      child: Row(children: <Widget>[
        Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = true;
                });
              },
            )),
        Text('Recordarme?', style: kLabelStyle)
      ]),
    );
  }

  Widget _BuildForgoutPassword() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => print('El usuario Presiiono que olvido la contraseña'),
        child: Text(
          'Olvidaste tu Contraseña?',
          style: kLabelStyle,
        ),
      ),
    );
  }

  Widget _BuildPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Contraseña',
          style: kLabelStyle,
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextFormField(
            onChanged: (text) {},
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "La contraseña es de caracter obligatorio";
              }
              return null;
            },
            autofocus: true,
            obscureText: true,
            keyboardType: TextInputType.multiline,
            style: const TextStyle(color: Colors.white, fontFamily: 'OpenSans'),
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.only(top: 14.0),
                prefixIcon: const Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
                hintText: '****',
                hintStyle: kHintTextStyle),
            onSaved: ((newValue) {
              widget._request.password = newValue!;
            }),
          ),
        )
      ],
    );
  }

  Widget _BuildLoginBtn() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: FloatingActionButton(
        backgroundColor: Colors.white,
        elevation: 5.0,
        onPressed: (() async {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();

            try {
              await widget._controller.controllerSignup(widget._request);
              try {
                dynamic date = await widget._controller
                    .getUserDates(widget._request.email!);
                // ignore: use_build_context_synchronously

                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Charges(
                        username: "Bienvenid@ ${widget._request.email}",
                        email: widget._request.email!,
                        userClients: date,
                      ),
                    ));
              } catch (e) {
                print("$e");
                // ignore: use_build_context_synchronously
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Charges(
                        username: "Bienvenid@ ${widget._request.email}",
                        email: widget._request.email!,
                        userClients: [
                          "No tiene Ningun Cliente Registrado",
                          "0",
                          "0",
                          "0"
                        ],
                      ),
                    ));
              }
              // ignore: use_build_context_synchronously

              // ignore: use_build_context_synchronously
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text('Iniciando Sesion'),
                backgroundColor: Color.fromARGB(255, 7, 255, 52),
              ));
            } catch (error) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(error.toString()),
                backgroundColor: const Color.fromARGB(255, 160, 0, 19),
              ));
            }
          }
        }),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        focusColor: Colors.white,
        child: Text(
          'INGRESAR',
          style: TextStyle(
            color: Colors.deepPurple[900],
            letterSpacing: 1.5,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _BuildLoginOr() {
    return Column(
      children: <Widget>[
        const Text(
          '-O-',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
        const SizedBox(
          height: 20.0,
        ),
        Text(
          'Registrate con',
          style: kLabelStyle,
        )
      ],
    );
  }

  Widget _BuildOtherLogin() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          GestureDetector(
            onTap: () => print("Login With Google"),
            child: Container(
              height: 60.0,
              width: 60.0,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/google.png"), //NetworkImage
                  scale: 3.0,
                ),
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 2),
                    blurRadius: 6.0,
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () => print("Login With Facebook"),
            child: Container(
              height: 60.0,
              width: 60.0,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image:
                      AssetImage("assets/images/Facebookk.png"), //NetworkImage
                  scale: 3.0,
                ),
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 2),
                    blurRadius: 6.0,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                Color.fromARGB(255, 165, 115, 245),
                Color.fromARGB(255, 171, 97, 241),
                Color.fromARGB(255, 124, 71, 224),
                Color.fromARGB(255, 132, 57, 229)
              ],
                  stops: [
                0.1,
                0.4,
                0.7,
                0.9
              ])),
        ),
        Container(
          height: double.infinity,
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(
              horizontal: 40.0,
              vertical: 120.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  "Bienvenido a DihApp",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 30.0,
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        _BuildEmail(),
                        const SizedBox(
                          height: 30.0,
                        ),
                        _BuildPassword(),
                        _BuildForgoutPassword(),
                        _BuildRememberCheckbox(),
                        _BuildLoginBtn(),
                        _BuildLoginOr(),
                        _BuildOtherLogin()
                      ],
                    )),
              ],
            ),
          ),
        )
      ],
    ));
  }
}
