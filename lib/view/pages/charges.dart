import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dihapp/model/entity/UserEntity.dart';
import 'package:dihapp/view/pages/clientstoday.dart';
import 'package:dihapp/view/widgets/Drawer_home.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math' as math;

import '../pages/sale.dart';

import 'package:flutter/material.dart';

import '../widgets/CustomSearchPersons.dart';

class Charges extends StatefulWidget {
  dynamic db = FirebaseFirestore.instance;
  late String username;

  String? email;
  get getEmail => this.email;

  set setEmail(email) => this.email = email;

//User clients extrae los clientes que tiene el cliente en la base de datos
  List? userClients;

  Charges(
      {super.key,
      required this.username,
      required this.email,
      required this.userClients});

  @override
  State<Charges> createState() => _ChargesState();
}

Stream<QuerySnapshot> returnClientsTimeStreamBuilder(String email, dynamic db) {
  dynamic userData =
      db.collection("users").doc(email).collection("Clientes").snapshots();

  return userData;
}

class _ChargesState extends State<Charges> {
  String servicio = "Netflix.png";

  Widget _BuildAppBarCharges() {
    return AppBar(
      elevation: 15,
      backgroundColor: const Color.fromARGB(255, 53, 0, 114),
      leadingWidth: 46,
      title: const Align(
        alignment: Alignment.topLeft,
        child: Text('Cobros'),
      ),
      actions: [
        Row(
          children: [
            IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: CustomSearchDelegate());
              },
            ),
            // IconButton(
            //   icon: const Icon(Icons.add),
            //   onPressed: () {
            //     print('Click en More');
            //   },
            // ),
          ],
        )
      ],
    );
  }

  List<String> searchTerms = [];
  List<int> colorCodes = <int>[50];
  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _clientsStream =
        returnClientsTimeStreamBuilder(widget.email!, widget.db);

    return StreamBuilder<QuerySnapshot>(
        stream: _clientsStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          List<UserEntityDates> lists = [];
          List<String> lists_id = [];
          snapshot.data?.docs.forEach((DocumentSnapshot doc) {
            lists.add(UserEntityDates.fromJson(
                doc.data() as Map<String, dynamic>, doc.reference.id));
            lists_id.add(doc.reference.id);

            //  userDates = UserEntityDates(cel: element["WhasApp"], cliente: element["Cliente"], precio: element["Precio"], servicio: element["Servicio"], typeSuscription: element["Tipo de Inscripcion"], fechaPago: element["Fecha Pago"]);
          });

          List<UserEntityDates> addPersons(List lists) {
            List<UserEntityDates> listPaymentsToday = [];
            lists.forEach((element) async {
              String fechaBD = element.returnStringFecha();
              if (fechaBD ==
                  "${DateTime.now().year}-${DateTime.now().month}-${DateTime.now().day}") {
                listPaymentsToday.add(element);
              }
            });
            return listPaymentsToday;
          }

          void showNotifications(String title, String body) async {
            {
              bool isallowed =
                  await AwesomeNotifications().isNotificationAllowed();
              if (!isallowed) {
                //no permission of local notification
                AwesomeNotifications().requestPermissionToSendNotifications();
              } else {
                //show notification
                AwesomeNotifications().createNotification(
                    content: NotificationContent(
                  //simgple notification
                  id: 123,
                  channelKey: 'basic', //set configuration wuth key "basic"
                  title: title,
                  body: body,
                ));
              }
            }
          }

          if (addPersons(lists).isNotEmpty) {
            showNotifications("Tienes Clientes por Cobrar hoy", ":) ");
          }
          if (snapshot.hasError) {
            return const Text("Error en la linea 134");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Text("Cargando...");
          }
          if (snapshot.data == null) {
            return Text("NO EXISTE");
          }

          return Scaffold(
              appBar: PreferredSize(
                preferredSize: const Size.fromHeight(50.0),
                child: _BuildAppBarCharges(),
              ),
              drawer: Drawerclass(
                nombreusuario: widget.username,
                email: widget.email!,
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height - 100,
                      child: ListView.builder(
                        padding: const EdgeInsets.all(0),
                        itemCount: lists.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {},
                            child: Card(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  ListTile(
                                    leading: const Icon(Icons.person),
                                    trailing: CircleAvatar(
                                      backgroundColor: Colors.white30,
                                      backgroundImage: AssetImage(
                                          "assets/images/${lists[index].getServicio}.png"),
                                    ),
                                    title: Text(
                                        'Cliente: ${lists[index].getCliente.toString()}'),
                                    subtitle: Text(
                                        'Fecha de pago: ${lists[index].getFechaPago.day}/${lists[index].getFechaPago.month}/${lists[index].getFechaPago.year}' +
                                            '\n' +
                                            'Valor: ${lists[index].getPrecio.toString()}'),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      TextButton(
                                        child: TextButton(
                                          onPressed: () {
                                            showDialog<String>(
                                                context: context,
                                                builder:
                                                    (BuildContext context) =>
                                                        AlertDialog(
                                                          title: const Text(
                                                              "Eliminar Cliente"),
                                                          content: Text(
                                                              "¿Estas Seguro de Eliminar a ${lists[index].getCliente} "),
                                                          actions: <Widget>[
                                                            TextButton(
                                                              onPressed: (() =>
                                                                  Navigator.pop(
                                                                      context,
                                                                      'Cancel')),
                                                              child: const Text(
                                                                  'Cancel',
                                                                  style: TextStyle(
                                                                      color: Color.fromARGB(
                                                                          255,
                                                                          130,
                                                                          0,
                                                                          0))),
                                                            ),
                                                            TextButton(
                                                                onPressed: (() {
                                                                  widget.db
                                                                      .collection(
                                                                          "users")
                                                                      .doc(widget
                                                                          .email)
                                                                      .collection(
                                                                          "Clientes")
                                                                      .doc(lists[
                                                                              index]
                                                                          .getIdCliente)
                                                                      .delete()
                                                                      .then(
                                                                          (doc) {
                                                                    ScaffoldMessenger.of(
                                                                            context)
                                                                        .showSnackBar(
                                                                            SnackBar(
                                                                      content: Text(
                                                                          'Cliente ${lists[index].getCliente} Eliminado'),
                                                                      backgroundColor:
                                                                          Color.fromARGB(
                                                                              255,
                                                                              255,
                                                                              7,
                                                                              7),
                                                                    ));
                                                                    Navigator.pop(
                                                                        context);
                                                                  }, onError: (e) => print("Error $e"));
                                                                }),
                                                                child: Text(
                                                                  'Eliminar',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .deepPurple[
                                                                          800]),
                                                                ))
                                                          ],
                                                        ));
                                          },
                                          child: Text(
                                            'Eliminar',
                                            style: TextStyle(
                                                color:
                                                    Colors.deepPurple.shade800),
                                          ),
                                        ),
                                        onPressed: () {/* ... */},
                                      ),
                                      const SizedBox(width: 8),
                                      TextButton(
                                        child: IconButton(
                                            onPressed: (() async {
                                              final Uri _url = Uri.parse(
                                                  'https://api.whatsapp.com/send/?phone=57${lists[index].getCel}');
                                              print(_url);
                                              if (!await launchUrl(_url,
                                                  mode: LaunchMode
                                                      .externalApplication)) {
                                                throw 'Could not launch $_url';
                                              }
                                            }),
                                            icon: const Icon(Icons.whatsapp,
                                                color: Colors.green)),
                                        onPressed: () {/* ... */},
                                      ),
                                      const SizedBox(width: 8),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButton: ExpandableFab(distance: 110.0, children: [
                ActionButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NewSale(
                                email: widget.email,
                              ))),
                  icon: const Icon(Icons.person_add),
                ),
                ActionButton(
                  onPressed: () {
                    // print(addPersons(lists)[0].getCliente);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentsToday(
                                  lists: addPersons(lists),
                                  username: widget.email,
                                )));
                  },
                  icon: const Icon(Icons.people),
                ),
              ]));
        });
  }
}

@immutable
class ExpandableFab extends StatefulWidget {
  const ExpandableFab({
    super.key,
    this.initialOpen,
    required this.distance,
    required this.children,
  });

  final bool? initialOpen;
  final double distance;
  final List<Widget> children;

  @override
  State<ExpandableFab> createState() => _ExpandableFabState();
}

class _ExpandableFabState extends State<ExpandableFab>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _expandAnimation;
  bool _open = false;

  @override
  void initState() {
    super.initState();
    _open = widget.initialOpen ?? false;
    _controller = AnimationController(
      value: _open ? 1.0 : 0.0,
      duration: const Duration(milliseconds: 250),
      vsync: this,
    );
    _expandAnimation = CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      reverseCurve: Curves.easeOutQuad,
      parent: _controller,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _toggle() {
    setState(() {
      _open = !_open;
      if (_open) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.bottomRight,
        clipBehavior: Clip.none,
        children: [
          _buildTapToCloseFab(),
          ..._buildExpandingActionButtons(),
          _buildTapToOpenFab(),
        ],
      ),
    );
  }

  Widget _buildTapToCloseFab() {
    return SizedBox(
      width: 56.0,
      height: 56.0,
      child: Center(
        child: Material(
          shape: const CircleBorder(),
          clipBehavior: Clip.antiAlias,
          elevation: 4.0,
          child: InkWell(
            onTap: _toggle,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.close,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildExpandingActionButtons() {
    final children = <Widget>[];
    final count = widget.children.length;
    final step = 90.0 / (count - 1);
    for (var i = 0, angleInDegrees = 0.0;
        i < count;
        i++, angleInDegrees += step) {
      children.add(
        _ExpandingActionButton(
          directionInDegrees: angleInDegrees,
          maxDistance: widget.distance,
          progress: _expandAnimation,
          child: widget.children[i],
        ),
      );
    }
    return children;
  }

  Widget _buildTapToOpenFab() {
    return IgnorePointer(
      ignoring: _open,
      child: AnimatedContainer(
        transformAlignment: Alignment.center,
        transform: Matrix4.diagonal3Values(
          _open ? 0.7 : 1.0,
          _open ? 0.7 : 1.0,
          1.0,
        ),
        duration: const Duration(milliseconds: 250),
        curve: const Interval(0.0, 0.5, curve: Curves.easeOut),
        child: AnimatedOpacity(
          opacity: _open ? 0.0 : 1.0,
          curve: const Interval(0.25, 1.0, curve: Curves.easeInOut),
          duration: const Duration(milliseconds: 250),
          child: FloatingActionButton(
            backgroundColor: Color.fromARGB(255, 128, 0, 192),
            onPressed: _toggle,
            child: const Icon(Icons.add_box),
          ),
        ),
      ),
    );
  }
}

@immutable
class _ExpandingActionButton extends StatelessWidget {
  const _ExpandingActionButton({
    required this.directionInDegrees,
    required this.maxDistance,
    required this.progress,
    required this.child,
  });

  final double directionInDegrees;
  final double maxDistance;
  final Animation<double> progress;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: progress,
      builder: (context, child) {
        final offset = Offset.fromDirection(
          directionInDegrees * (math.pi / 180.0),
          progress.value * maxDistance,
        );
        return Positioned(
          right: 4.0 + offset.dx,
          bottom: 4.0 + offset.dy,
          child: Transform.rotate(
            angle: (1.0 - progress.value) * math.pi / 2,
            child: child!,
          ),
        );
      },
      child: FadeTransition(
        opacity: progress,
        child: child,
      ),
    );
  }
}

@immutable
class ActionButton extends StatelessWidget {
  const ActionButton({
    super.key,
    this.onPressed,
    required this.icon,
  });

  final VoidCallback? onPressed;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Material(
      shape: const CircleBorder(),
      clipBehavior: Clip.antiAlias,
      color: const Color.fromARGB(255, 129, 0, 227),
      elevation: 5.0,
      child: IconButton(
        onPressed: onPressed,
        icon: icon,
        color: theme.colorScheme.onSecondary,
      ),
    );
  }
}

@immutable
class FakeItem extends StatelessWidget {
  const FakeItem({
    super.key,
    required this.isBig,
  });

  final bool isBig;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      height: isBig ? 128.0 : 36.0,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey.shade300,
      ),
    );
  }
}
