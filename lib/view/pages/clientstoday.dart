import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:dihapp/controller/request/UpdatePaymentController.dart';
import 'package:dihapp/model/entity/UserEntity.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PaymentsToday extends StatefulWidget {
  List<UserEntityDates> lists = [];
  int? type_Inscription;
  late UpdatePaymentRequest _updatePayment;
  String? username;
  PaymentsToday({super.key, required this.lists, this.username}) {
    _updatePayment = UpdatePaymentRequest();
  }

  @override
  State<PaymentsToday> createState() => _PaymentsTodayState();
}

class _PaymentsTodayState extends State<PaymentsToday> {
  @override
  Widget build(BuildContext context) {
    ;
    if (widget.lists.length <= 0) {
      return Scaffold(
        appBar: AppBar(
            title: Text("Clientes"),
            backgroundColor: const Color.fromARGB(255, 53, 0, 114)),
        body: Center(child: Text("No hay ningun Cliente para cobrar hoy")),
      );
    }
    return Scaffold(
        appBar: AppBar(
            title: Text("Clientes Hoy"),
            backgroundColor: const Color.fromARGB(255, 53, 0, 114)),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: const Text(
                          "Clientes a Cobrar Hoy",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'OpenSans'),
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(0),
                        itemCount: widget.lists.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              print(widget.lists[index].getIdCliente);
                            },
                            child: Card(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  ListTile(
                                    leading: const Icon(Icons.person),
                                    trailing: CircleAvatar(
                                      backgroundColor: Colors.white30,
                                      backgroundImage: AssetImage(
                                          "assets/images/${widget.lists[index].getServicio}.png"),
                                    ),
                                    title: Text(
                                        'Cliente: ${widget.lists[index].getCliente.toString()}'),
                                    subtitle: Text(
                                        'Fecha de pago: ${widget.lists[index].getFechaPago.day}/${widget.lists[index].getFechaPago.month}/${widget.lists[index].getFechaPago.year}' +
                                            '\n' +
                                            'Valor: ${widget.lists[index].getPrecio.toString()}'),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      TextButton(
                                        child: TextButton(
                                          onPressed: () {
                                            setState(() {
                                              switch (widget.lists[index]
                                                  .getTypeSuscription) {
                                                case 'Semanal':
                                                  setState(() {
                                                    widget.type_Inscription = 7;
                                                  });
                                                  break;
                                                case 'Mensual':
                                                  setState(() {
                                                    widget.type_Inscription =
                                                        30;
                                                  });
                                                  break;
                                                case 'Anual':
                                                  setState(() {
                                                    widget.type_Inscription =
                                                        365;
                                                  });
                                              }
                                            });

                                            showDialog<String>(
                                                context: context,
                                                builder:
                                                    (BuildContext context) =>
                                                        AlertDialog(
                                                          title: const Text(
                                                              "Actualizar Pago"),
                                                          content: Text(
                                                              "¿El proximo pago de ${widget.lists[index].getCliente} se actualizara para el ${widget.lists[index].getFechaPago.add(Duration(days: widget.type_Inscription!))}"),
                                                          actions: <Widget>[
                                                            TextButton(
                                                              onPressed: (() =>
                                                                  Navigator.pop(
                                                                      context,
                                                                      'Cancel')),
                                                              child: const Text(
                                                                  'Cancel',
                                                                  style: TextStyle(
                                                                      color: Color.fromARGB(
                                                                          255,
                                                                          130,
                                                                          0,
                                                                          0))),
                                                            ),
                                                            TextButton(
                                                                onPressed:
                                                                    (() async {
                                                                  print(widget
                                                                      .username);
                                                                  try {
                                                                    dynamic result = widget._updatePayment.updatePaymentRequestController(
                                                                        await widget
                                                                            .lists[
                                                                                index]
                                                                            .getFechaPago
                                                                            .add(Duration(
                                                                                days: widget
                                                                                    .type_Inscription!)),
                                                                        widget
                                                                            .username!,
                                                                        widget
                                                                            .lists[index]
                                                                            .getIdCliente);
                                                                    Navigator.pop(
                                                                        context);
                                                                    Navigator.pop(
                                                                        context);
                                                                  } catch (e) {
                                                                    print(e);
                                                                  }
                                                                }),
                                                                child: Text(
                                                                  'Si, Actualizar Pago',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .deepPurple[
                                                                          800]),
                                                                ))
                                                          ],
                                                        ));
                                          },
                                          child: Text(
                                            '¿Realizo el pago?',
                                            style: TextStyle(
                                                color:
                                                    Colors.deepPurple.shade800),
                                          ),
                                        ),
                                        onPressed: () {/* ... */},
                                      ),
                                      const SizedBox(width: 8),
                                      TextButton(
                                        child: IconButton(
                                            onPressed: (() async {
                                              final Uri _url = Uri.parse(
                                                  'https://api.whatsapp.com/send/?phone=57${widget.lists[index].getCel}');
                                              print(_url);
                                              if (!await launchUrl(_url,
                                                  mode: LaunchMode
                                                      .externalApplication)) {
                                                throw 'Could not launch $_url';
                                              }
                                            }),
                                            icon: const Icon(Icons.whatsapp,
                                                color: Colors.green)),
                                        onPressed: () {/* ... */},
                                      ),
                                      const SizedBox(width: 8),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
