import 'package:dihapp/model/entity/user.dart';
import 'package:dihapp/view/pages/charges.dart';
import 'package:flutter/material.dart';

import '../../model/entity/UserEntity.dart';

class Drawerclass extends StatelessWidget {
  final String nombreusuario;
  final String email;
  late UserEntityDates _userClients;
  Drawerclass({super.key, required this.nombreusuario, required this.email});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(nombreusuario),
            accountEmail: Text(email),
            currentAccountPicture: const CircleAvatar(
              backgroundImage: NetworkImage(
                "https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes.png",
              ),
            ),
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                  "https://wallpaperaccess.com/full/1239386.jpg",
                ),
                fit: BoxFit.fill,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text("Inicio"),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.monetization_on),
            title: Text("Cobros"),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Charges(
                            username: "Prueba desde drawer home",
                            email: "Prueba desde drawer.dart",
                            userClients: [],
                          )));
            },
          ),
          ListTile(
            leading: Icon(Icons.production_quantity_limits),
            title: Text("Productos"),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.contact_mail),
            title: Text("Contactame"),
            onTap: () {},
          )
        ],
      ),
    );
  }
}
