import 'package:flutter/material.dart';

class AppbarInsert extends StatelessWidget {
  String title;
  AppbarInsert({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Center(child: Text(title)),

        backgroundColor: const Color.fromARGB(255, 53, 0, 114));
  }
}
